package com.example.test.test.Tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@RunWith(SpringRunner.class)
public class PeriodControllerIntegrationTest {

    @Test
    public void testCreatePeriodEndpoint() {
        given()
                .contentType("application/json")
                .body("{ \"slotType\": \"UNDEFINED\", \"slot\": { \"id\": \"6f0bca5fe7da43b886e707b10f1fd81\" }, \"schedule\": { \"id\": \"86a973e04162486faed1ffd2d5455c87\" }, \"administrator\": { \"id\": \"4ba5dcfb8f084db4875079046f8bfe0e\" } }")
                .when()
                .post("http://localhost:8080/api/period/create")
                .then()
                .assertThat()
                .body("message", equalTo("Slot with id 6f0bca5fe7da43b886e707b10f1fd81 not exist"))
                .body("status", equalTo(404));
    }

    
}

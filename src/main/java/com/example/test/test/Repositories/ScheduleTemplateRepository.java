package com.example.test.test.Repositories;

import com.example.test.test.Models.Entities.ScheduleTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ScheduleTemplateRepository extends JpaRepository<ScheduleTemplate, String> {

}
package com.example.test.test.Models.Entities;

import com.example.test.test.Models.Enums.SlotType;
import com.example.test.test.Utils.UUIDConverter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Data;

import java.util.UUID;

@Entity
@Data
@Table(name = "periods")
public class Period {

    @Id
    private String id;

    @PrePersist
    public void prePersist() {
        if (this.id == null) {
            UUIDConverter uuidConverter = new UUIDConverter();
            //На практике это означает, что даже при генерации миллиардов UUID в секунду
            //вероятность коллизии остается настолько ничтожно малой, что ей можно пренебречь.
            this.id = uuidConverter.convertToDatabaseColumn(UUID.randomUUID());
        }
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "slot_type", nullable = false)
    private SlotType slotType;

    @ManyToOne
    @JoinColumn(name = "slot_id", nullable = false)
    private Slot slot;

    @ManyToOne
    @JoinColumn(name = "schedule_id", nullable = false)
    @JsonIgnoreProperties("periods")
    private Schedule schedule;

    @ManyToOne
    @JoinColumn(name = "administrator_id", nullable = false)
    private Employee administrator;

    @ManyToOne
    @JoinColumn(name = "executor_id")
    private Employee executor;
}

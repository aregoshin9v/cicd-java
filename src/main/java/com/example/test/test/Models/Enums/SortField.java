package com.example.test.test.Models.Enums;

import lombok.Getter;

@Getter
public enum SortField {

    BEGIN_TIME,
    END_TIME,
    SLOT_TYPE,
    ADMINISTRATOR_ID,
    EXECUTOR_ID;
}

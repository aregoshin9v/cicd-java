package com.example.test.test.Models.Enums;

import lombok.Getter;

@Getter
public enum SortDirection {

    ASC,
    DESC;
}
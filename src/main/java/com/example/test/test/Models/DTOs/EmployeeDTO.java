package com.example.test.test.Models.DTOs;

import com.example.test.test.Models.Enums.EmployeePosition;
import com.example.test.test.Models.Enums.EmployeeStatus;


public class EmployeeDTO {

    private String name;

    private EmployeeStatus status;

    private EmployeePosition position;
}

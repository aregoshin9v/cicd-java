package com.example.test.test.ExceptionsHandling.Exceptions;

public class OverlappingPeriodsException extends Exception{

    public OverlappingPeriodsException(String message){
        super(message);
    }
}

package com.example.test.test.ExceptionsHandling.Exceptions;

public class NotFoundException extends Exception{
    public NotFoundException(String message){
        super(message);
    }
}

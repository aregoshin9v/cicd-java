package com.example.test.test.ExceptionsHandling.Exceptions;

public class TimeRangeException extends Exception{

    public TimeRangeException(String message){
        super(message);
    }
}

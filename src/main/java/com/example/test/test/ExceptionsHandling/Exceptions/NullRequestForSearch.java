package com.example.test.test.ExceptionsHandling.Exceptions;

public class NullRequestForSearch extends Exception{

    public NullRequestForSearch(String message){
        super(message);
    }
}
